FROM stanfordnmbl/opensim-cpp:4.5

LABEL maintainer="Roger van Kanten"

ENV DEBIAN_FRONTEND=noninteractive

# Install project dependencies
RUN apt-get update -q && \
    apt-get install -y libboost-all-dev xsdcxx libglew-dev qt5-default libxerces-c-dev libqt5opengl5-dev \
    libeigen3-dev curl zip unzip tar ca-certificates gpg wget --no-install-recommends

# Downloads and compiles prerequisite library pagmo2
RUN git clone https://github.com/Microsoft/vcpkg.git ~/vcpkg && ~/vcpkg/bootstrap-vcpkg.sh && ~/vcpkg/vcpkg install pagmo2

# Add vcpkg directory to path so `find_package` commands will succeed
ENV PATH=$PATH:/root/vcpkg/installed/x64-linux

# Get CMake
RUN wget -qO- "https://github.com/Kitware/CMake/releases/download/v3.30.5/cmake-3.30.5-linux-x86_64.tar.gz" | \
    tar --strip-components=1 -xz -C /usr/local
