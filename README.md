# CEINMS-RT Docker Image #

This repository contains the Dockerfile for the container to build CEINMS.

This image will not yet contain CEINMS itself.

This image is built automatically at https://hub.docker.com/repository/docker/be1et/ceinms-platform

Use with:

```shell
docker pull be1et/ceinms-platform
```
